import { Component, ElementRef, ViewChild } from "@angular/core";
import { alert } from "tns-core-modules/ui/dialogs";
import { Page } from "tns-core-modules/ui/page";
import { LoadingIndicator } from 'nativescript-loading-indicator';

import { UserService } from "../shared/user.service";
import { RouterExtensions } from "nativescript-angular/router";
import { LoginUser } from "~/shared/login-user.model";

@Component({
    selector: "app-login",
    moduleId: module.id,
    templateUrl: "./login.component.html",
    styleUrls: ['./login.component.css']
})
export class LoginComponent {
    user: LoginUser;
    loader: LoadingIndicator;
    @ViewChild("password") passwordElement: ElementRef;

    constructor(private page: Page, private userService: UserService, private router: RouterExtensions) {
        this.page.actionBarHidden = true;
        this.loader = new LoadingIndicator();
        this.user = new LoginUser();
    }

    submit() {
        if (!this.user.username || !this.user.password) {
            this.alert("Por favor ingresa tu nombre de usuario y contraseña");
            return;
        }

        this.login();
    }

    ngOnInit(): void {
        /*this.user.username = "admin";
        this.user.password = "admin";
        this.login();*/
    }

    showLoader() {
        const options: object = {
            message: 'Cargando...',
            progress: 0.65,
            android: {
                indeterminate: true,
                cancelable: true,
                cancelListener: (dialog) => console.log("Loading cancelled"),
                max: 100,
                progressNumberFormat: "%1d/%2d",
                progressPercentFormat: 0.53,
                progressStyle: 1,
                secondaryProgress: 1
            }
        };
        this.loader.show(options);
    }

    hideLoader() {
        this.loader.hide();
    }

    login() {
        this.showLoader();
        this.userService.login(this.user.username, this.user.password)
            .then((response) => {
                this.hideLoader();
                this.router.navigate(["/home"], { clearHistory: true });
            })
            .catch((err) => {
                this.hideLoader();
                this.alert(err.toString());
            });
    }

    focusPassword() {
        this.passwordElement.nativeElement.focus();
    }

    alert(message: string) {
        return alert({
            title: "EasyTicket",
            okButtonText: "OK",
            message: message
        });
    }
}

