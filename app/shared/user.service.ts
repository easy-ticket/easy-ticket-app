import { Injectable } from "@angular/core";
import { Kinvey } from "kinvey-nativescript-sdk";
import { Token } from "./token.model";
import { API_URL } from "../constants";
import { Organizer } from "./organizer.model";

@Injectable()
export class UserService {
    currentToken: Token;
    currentOrganizer: Organizer;

    logout(): void {
        this.currentToken = null;
    }

    getCurrentOrganizer() {
        return this.currentOrganizer;
    }

    getCurrentToken(): Token {
        return this.currentToken;
    }

    handleErrors(error: Kinvey.BaseError) {
        console.error(error.message);
    }

    getHeaders(auth?: boolean) {
        const headers: Headers = new Headers();
        headers.append('Content-Type', 'application/json');
        if (this.getCurrentToken() != null && auth === true) {
            headers.append('Authorization', `Token: ${this.getCurrentToken().token}`);
        }
        console.log("Headers: " + JSON.stringify(headers));
        return headers;
    }

    getOrganizer(username: string): Promise<Organizer> {
        return new Promise((resolve, reject) => {
            const url = `${API_URL}/user/organizer/${username}`;

            const options: object = {
                method: 'GET',
                mode: 'cors',
                cache: 'default',
                headers: this.getHeaders(true)
            };

            fetch(url, options).
                then(response => {
                    return response.json();
                }).then(organizer => {
                    console.log(JSON.stringify(organizer));
                    if (organizer["user"] != null && organizer["user"]["username"] != null) {
                        const user = new Organizer(
                            organizer['user']['email'],
                            organizer['user']['username'],
                            organizer['user']['first_name'],
                            organizer['user']['last_name'],
                            organizer['user']['password'],
                            organizer['contact_email'],
                            organizer['contact_name'],
                            organizer['nit'],
                            organizer['phone'],
                        );
                        this.currentOrganizer = user;
                        resolve(this.currentOrganizer);
                    } else {
                        this.currentOrganizer = null;
                        reject("Invalid data");
                    }
                }).catch(err => {
                    this.handleErrors(err);
                    reject("Error al conectar al servidor.");
                });

        });
    }

    login(username: string, password: string): Promise<any> {
        this.logout();

        const url = `${API_URL}/token/`;
        const body = {
            username,
            password,
        };

        const headers: Headers = new Headers();
        headers.append('Content-Type', 'application/json');

        const options: object = {
            method: 'POST',
            mode: 'cors',
            cache: 'default',
            headers,
            body: JSON.stringify(body)
        };

        return new Promise((resolve, reject) => {
            fetch(url, options).
                then(response => {
                    return response.json();
                }).then(response => {
                    //console.log(JSON.stringify(response));
                    if (response["token"] != null && response["role"] != null) {
                        if (response["role"] === "organizer") {
                            this.currentToken = new Token(response["token"], response["role"]);
                            this.getOrganizer(username)
                                .then((organizer) => {
                                    resolve(organizer);
                                }).catch(() => {
                                    reject("Error al obtener los datos del usuario.");
                                });

                        } else {
                            reject("Esta cuenta no posee los permisos necesarios.")
                        }
                    } else if (response["non_field_errors"] != null) {
                        reject(response["non_field_errors"]);
                    }
                }).catch(err => {
                    this.handleErrors(err);
                    reject("Error al conectar al servidor.");
                });
        });
    }
}
