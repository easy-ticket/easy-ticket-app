import { Injectable } from "@angular/core";
import { LAMBDA_URL, LAMBDA_KEY } from "../constants";

@Injectable()
export class ValidateService {

    validateRaw(body: object): Promise<string> {
        const headers: Headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('x-api-key', LAMBDA_KEY);

        const options: object = {
            method: 'POST',
            mode: 'cors',
            cache: 'default',
            headers,
            body: JSON.stringify(body)
        };

        return new Promise((resolve, reject) => {
            fetch(LAMBDA_URL, options).
                then(response => {
                    return response.json();
                })
                .then(response => {
                    console.log(JSON.stringify(response));
                    if (response["response"] != null) {
                        resolve(response["response"]);
                    } else {
                        reject("Datos inválidos.")
                    }
                })
                .catch(err => {
                    reject(err);
                });
        });
    }
    validateQr(eventId: number, hash: string, documentNumber: string): Promise<string> {
        const url = `${LAMBDA_URL}/token/`;
        const body = {
            "document_number": documentNumber,
            "event_id": eventId,
            hash
        };
        const headers: Headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('x-api-key', LAMBDA_KEY);

        const options: object = {
            method: 'POST',
            mode: 'cors',
            cache: 'default',
            headers,
            body: JSON.stringify(body)
        };

        return new Promise((resolve, reject) => {
            fetch(url, options).
                then(response => {
                    return response.json();
                })
                .then(response => {
                    console.log(JSON.stringify(response));
                    if (response["response"] != null) {
                        resolve(response["response"]);
                    } else {
                        reject("Datos inválidos.")
                    }
                })
                .catch(err => {
                    reject(err);
                });
        });
    }

    validatePdf(eventId: number, documentNumber: string): Promise<string> {
        const url = `${LAMBDA_URL}/token/`;
        const body = {
            "document_number": documentNumber,
            "event_id": eventId
        };
        const headers: Headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('x-api-key', LAMBDA_KEY);

        const options: object = {
            method: 'POST',
            mode: 'cors',
            cache: 'default',
            headers,
            body: JSON.stringify(body)
        };

        return new Promise((resolve, reject) => {
            fetch(url, options).
                then(response => {
                    return response.json();
                })
                .then(response => {
                    console.log(JSON.stringify(response));
                    if (response["response"] != null) {
                        resolve(response["response"]);
                    } else {
                        reject("Datos inválidos.")
                    }
                })
                .catch(err => {
                    reject(err);
                });
        });
    }
}