import { Injectable } from "@angular/core";
import { UserService } from "./user.service";
import { API_URL } from "~/constants";
import { Event } from "./event.model";

@Injectable()
export class EventService {
    constructor(private userService: UserService) {
    }

    getEventsForCurrentUser(): Promise<Event[]> {
        return new Promise((resolve, reject) => {
            const organizer = this.userService.getCurrentOrganizer();
            if (organizer == null || organizer.username == null) {
                reject("Usuario inválido.")
            }
            const url = `${API_URL}/user/organizer/${organizer.username}/event/`;

            const options: object = {
                method: 'GET',
                mode: 'cors',
                cache: 'default',
                headers: this.userService.getHeaders(true)
            };

            fetch(url, options).
                then(response => {
                    return response.json();
                }).then(events => {
                    console.log(JSON.stringify(events));
                    resolve(events);
                }).catch(err => {
                    reject(err);
                });
        });
    }
}