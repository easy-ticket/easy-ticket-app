import { Component, OnInit } from "@angular/core";
import { BarcodeScanner } from "nativescript-barcodescanner";
import { RouterExtensions } from "nativescript-angular/router";
import { UserService } from "../shared/user.service";
import { EventService } from "~/shared/event.service";
import { LoadingIndicator } from "nativescript-loading-indicator";
import { Event } from "../shared/event.model";
import { ValidateService } from "~/shared/validate.service";

const API_URL = '';
const API_SECRET_KEY = '';

@Component({
    selector: "app-home",
    moduleId: module.id,
    templateUrl: "./home.component.html",
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
    title = "";
    loader: LoadingIndicator;
    events: Event[];
    currentEvent: Event;

    constructor(private barcodeScanner: BarcodeScanner, private router: RouterExtensions, private userService: UserService, private eventService: EventService, private validateService: ValidateService) {
        this.title = userService.getCurrentOrganizer().contact_name;
        this.loader = new LoadingIndicator();
        this.events = [];
        this.currentEvent = null;
    }

    getSelectedEventName(): string {
        if (this.currentEvent != null) {
            return this.currentEvent.name;
        }
        return "";
    }

    isSelectingEvent(): boolean {
        return this.currentEvent == null;
    }

    unSelectEvent() {
        this.currentEvent = null;
    }

    public onItemTap(args: any) {
        this.currentEvent = this.events[args.index];
    }

    alert(message: string) {
        return alert({
            title: "EasyTicket",
            okButtonText: "OK",
            message: message
        });
    }

    ngOnInit(): void {
        this.showLoader("Cargando eventos...");
        this.eventService.getEventsForCurrentUser().then(events => {
            this.events = events;
            this.hideLoader();
        }).catch(err => {
            alert("Error obteniendo los eventos.");
            this.doLogout();
        });
    }

    showLoader(message: string) {
        const options: object = {
            message,
            progress: 0.65,
            android: {
                indeterminate: true,
                cancelable: false,
                cancelListener: (dialog) => console.log("Loading cancelled"),
                max: 100,
                progressNumberFormat: "%1d/%2d",
                progressPercentFormat: 0.53,
                progressStyle: 1,
                secondaryProgress: 1
            }
        };
        this.loader.show(options);
    }

    hideLoader() {
        this.loader.hide();
    }

    public doScanPdf417() {
        this.scan(false, false, false, undefined, "PDF_417");
    }

    public doScanQr() {
        this.scan(false, false, false, undefined, "QR_CODE");
    }

    public doLogout() {
        this.userService.logout();
        this.router.navigate(["/"], { clearHistory: true });
    }

    private handleResponse(response: string) {
        if (response === "ok") {
            return this.alert("¡Código correcto!");
        } else if (response === "already_in_use") {
            return this.alert("El código ya ha sido utilizado anteriormente.");
        }
        return this.alert("El código no es válido");
    }

    private handleQrScan(result) {
        const data = JSON.parse(result.text);
        if (data["document_number"] != null && data["event_id"] != null && data["hash"] != null) {
            //if (confirm("¿La cédula corresponde a " + data["document_number"] + " ?")) {
                this.showLoader("Validando código...");
                this.validateService.validateRaw(data)
                    .then(response => {
                        this.hideLoader();
                        this.handleResponse(response);
                    }).catch(err => {
                        this.hideLoader();
                        this.alert("Error al validar el código: " + err);
                    });
            //}
        } else {
            this.alert("Código inválido");
        }

    }

    private handlePdf417Scan(result) {
        const pdfData = result.text;
        const cedula = pdfData.substring(48, 58);
        if (confirm("¿La cédula corresponde a " + cedula + " ?")) {
            this.showLoader("Validando cédula...");
            this.validateService.validatePdf(this.currentEvent.id, cedula)
                .then(response => {
                    this.hideLoader();
                    this.handleResponse(response);
                }).catch(err => {
                    this.hideLoader();
                    this.alert("Error al validar el código: " + err);
                });
        }
    }

    private scan(front: boolean, flip: boolean, torch?: boolean, orientation?: string, formats?: string) {
        this.barcodeScanner.scan({
            formats,
            cancelLabel: "EXIT. Also, try the volume buttons!", // iOS only, default 'Close'
            cancelLabelBackgroundColor: "#333333", // iOS only, default '#000000' (black)
            message: "Ubique el código dentro del visor", // Android only, default is 'Place a barcode inside the viewfinder rectangle to scan it.'
            preferFrontCamera: front,     // Android only, default false
            showFlipCameraButton: flip,   // default false
            showTorchButton: torch,       // iOS only, default false
            torchOn: false,               // launch with the flashlight on (default false)
            resultDisplayDuration: 500,   // Android only, default 1500 (ms), set to 0 to disable echoing the scanned text
            orientation: orientation,     // Android only, default undefined (sensor-driven orientation), other options: portrait|landscape
            beepOnScan: true,             // Play or Suppress beep on scan (default true)
            openSettingsIfPermissionWasPreviouslyDenied: true, // On iOS you can send the user to the settings app if access was previously denied
            closeCallback: () => {
                console.log("Scanner closed @ " + new Date().getTime());
            }
        }).then(
            (result) => {
                console.log("--- scanned: " + result.text);
                // Note that this Promise is never invoked when a 'continuousScanCallback' function is provided
                setTimeout(() => {
                    if (formats === "PDF_417") {
                        this.handlePdf417Scan(result);
                        console.log("PDF FOUND");
                    } else if (formats === "QR_CODE") {
                        console.log("QR FOUND");
                        this.handleQrScan(result);
                    } else {
                        console.log("NOTHING FOUND");
                    }
                }, 500);
            },
            (errorMessage) => {
                console.log("No scan. " + errorMessage);
            }
        );
    }
}
